<?php

namespace app\controllers;

use app\models\ProductCategory;
use app\models\Product;
use app\models\Cart;
use app\models\Shop;

class ShopController extends \yii\web\Controller
{
	# магазин
    public function actionShop($id=0)
    {
    	return $this->render('shop',['categories' => $this->formatedCategory(),
    								 'products' => Product::findall(['id_product_category'=>$id])
        								]);
    }

    # Корзина
    public function actionCart ()
    {
        $cart = (new Shop())->getCart();
        $products = Product::find()->where(['in', 'id', array_keys($cart)])->all();

        return $this->render('cart', ['products' => $products,
                                      'carts' => $cart]);
    }

    # ajax add cart
    public function actionAddcart ()
    {
    	
    	if(\Yii::$app->request->isAjax){
            $post = \Yii::$app->request->post();
            if (\Yii::$app->user->isGuest) {
                $quantity = Shop::saveCartCookie($post['id']*1);
            } else {
                $quantity = Shop::saveCartDb($post['id']*1);                
            }
    		
    	}
    	return json_encode(['quantity'=>$quantity]);
    }

    protected function formatedCategory()
    {
    	$cat = ProductCategory::find(['active' => 1])->orderBy('parent_id')->all();
    	$new_cat = [];
    	foreach ($cat as $value) {
    		if ($value->parent_id == 0) {
    			$new_cat[] = $value;
    			$this->recursiveCategory($value->id, 1, $cat, $new_cat);
    		}
    	}
    	return $new_cat;
    }

    private function recursiveCategory($id, $count, $mass, &$new_cat)
    {
    	$count++;
    	foreach ($mass as $value) {
    		if ($value->parent_id == $id) {
    			$value->title = str_repeat("&emsp;", $count).$value->title;
    			$new_cat[] = $value;
    			$this->recursiveCategory($value->id, $count, $mass, $new_cat);
    		}
    	}
    }

}
