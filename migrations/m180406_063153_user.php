<?php

use yii\db\Migration;

/**
 * Class m180406_063153_user
 */
class m180406_063153_user extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('user', [
            'id' => 'pk',
            'username' => 'varchar(255) NOT NULL',
            'auth_key' => 'varchar(32) NOT NULL',
            'password_hash' => 'varchar(255) NOT NULL',
            'confirmation_token' => 'varchar(255) DEFAULT NULL',
            'status' => 'int(11) NOT NULL DEFAULT "1"',
            'superadmin' => 'smallint(6) DEFAULT "0"',
            'created_at' => 'int(11) NOT NULL',
            'updated_at' => 'int(11) NOT NULL',
            'registration_ip' => 'varchar(15) DEFAULT NULL',
            'bind_to_ip' => 'varchar(255) DEFAULT NULL',
            'email' => 'varchar(128) DEFAULT NULL',
            'email_confirmed' => 'smallint(1) NOT NULL DEFAULT "0"',
            'confirmed_at' => 'int(11) DEFAULT NULL',
            'unconfirmed_email' => 'varchar(255) DEFAULT NULL',
            'blocked_at' => 'int(11) DEFAULT NULL',
            'flags' => 'int(11) DEFAULT NULL',
            'last_login_at' => 'int(11) DEFAULT NULL',
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('user');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180406_063153_user cannot be reverted.\n";

        return false;
    }
    */
}
