<?php

use yii\db\Migration;

/**
 * Class m180406_062202_product_category
 */
class m180406_062202_product_category extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('product_category', [
            'id' => 'pk',
            'parent_id' => 'INT(11) NOT NULL DEFAULT "0"',
            'uri' => 'VARCHAR(50) NOT NULL DEFAULT ""',
            'title' => 'VARCHAR(50) NOT NULL DEFAULT ""',
            'icon' => 'VARCHAR(50) NOT NULL DEFAULT ""',
            'active' => 'TINYINT(1) NOT NULL DEFAULT "0"',
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('product_category');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180406_062202_category_product cannot be reverted.\n";

        return false;
    }
    */
}
