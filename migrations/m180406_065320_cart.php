<?php

use yii\db\Migration;

/**
 * Class m180406_065320_cart
 */
class m180406_065320_cart extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('cart', [
            'id' => 'pk',
            'id_user' => 'INT(11) NOT NULL',
            'products' => 'text NOT NULL DEFAULT ""',
            'id_product' => 'VARCHAR(50) NOT NULL DEFAULT ""',
            'quantity' => 'VARCHAR(50) NOT NULL DEFAULT ""',
            'date_create' => 'VARCHAR(50) NOT NULL DEFAULT ""',
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('cart');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180406_065320_cart cannot be reverted.\n";

        return false;
    }
    */
}
