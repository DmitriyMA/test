<?php
namespace app\helpers;

use Yii;
use app\models\Cart;
use app\models\Shop;

class MyUtils
{
	public static function getCountCart()
	{ 
		$cookies = \Yii::$app->request->cookies;

		if (Yii::$app->user->isGuest) {
	        $quantity = 0;
	        if (($cookie = $cookies->get('quantity')) !== null) {
	            $quantity = $cookie->value;
	        }
		} else {
			$products = Shop::getProductsCookie();
	        if (!empty($products)) {
	        // if (($cookie = $cookies->get('products')) !== null) {
	            // $products = json_decode($cookie->value, true);
	            $quantity = Shop::saveCartDb($products);
	            Shop::cleanCartCookie();


			    // $cookies2 = \Yii::$app->response->cookies;
			    // $cookies2->remove('products');
			    // $cookies2->remove('quantity');
	        } else {
	        	$quantity = Cart::findOne(['id_user'=>Yii::$app->user->identity->id])->quantity;
	        	if (is_null($quantity)) $quantity = 0;
	        }
		}

		return $quantity;
	}

}