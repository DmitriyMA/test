<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "cart".
 *
 * @property int $id
 * @property int $id_user
 * @property string $products
 * @property string $id_product
 * @property string $quantity
 * @property string $date_create
 */
class Cart extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'cart';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_user', 'products'], 'required'],
            [['id_user'], 'integer'],
            [['products'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_user' => 'Id User',
            'products' => 'Products',
            'quantity' => 'Quantity',
        ];
    }
}
