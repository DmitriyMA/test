<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "product_category".
 *
 * @property int $id
 * @property int $parent_id
 * @property string $uri
 * @property string $title
 * @property string $icon
 * @property int $active
 */
class ProductCategory extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'product_category';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['parent_id'], 'integer'],
            [['uri', 'title', 'icon'], 'string', 'max' => 50],
            [['active'], 'string', 'max' => 1],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'parent_id' => 'Parent ID',
            'uri' => 'Uri',
            'title' => 'Title',
            'icon' => 'Icon',
            'active' => 'Active',
        ];
    }
}
