<?php

namespace app\models;

use Yii;
use app\models\Cart;

class Shop
{
    public function saveCartDb($id)
    {
        $products = [];
        $cart = Cart::findOne(['id_user'=>\Yii::$app->user->identity->id]);
        if (is_null($cart->id)) {
            $cart = new Cart();
            $cart->id_user = \Yii::$app->user->identity->id;
        } else {
            $products = json_decode($cart->products, true);
        }
        
        if (is_array($id)) { #если это массив от кук
            foreach ($id as $key => $value) {
                if (array_key_exists($key, $products)) {
                    $products[$key] += $value;
                } else {
                    $products[$key] = $value;
                }
            }
        } else { #если это одна штука
            if (array_key_exists($id, $products)) {
                $products[$id]++;
            } else {
                $products[$id] = 1;
            }
        }

        $quantity = array_sum($products);
        $cart->quantity = $quantity;
        
        $cart->products = json_encode($products);
        $cart->save();

        return $quantity;
    }

    public function saveCartCookie($id)
    {
        $cookies = \Yii::$app->request->cookies;
        $products = [];
        if (($cookie = $cookies->get('products')) !== null) {
            $products = json_decode($cookie->value, true);
        }

        if (is_array($products) && array_key_exists($id, $products)) {
            $products[$id]++;
        } else {
            $products[$id] = 1;
        }

        $cookies2 = \Yii::$app->response->cookies;
        $cookies2->add(new \yii\web\Cookie([
            'name' => 'products',
            'value' => json_encode($products),
        ]));
        $quantity = array_sum($products);
        $cookies2->add(new \yii\web\Cookie([
            'name' => 'quantity',
            'value' => $quantity,
        ]));

        return $quantity;
    }

    public function getCart()
    {
        if (Yii::$app->user->isGuest) {
            $products = $this->getProductsCookie();
        } else {
            $products = $this->getProductsDb();
        }

        return $products;
    }

    public function getProductsDb ()
    {
        $products = [];
        $cart = Cart::findOne(['id_user'=>\Yii::$app->user->identity->id]);
        if (!is_null($cart->products)) {
            $products = json_decode($cart->products, true);
        }

        return $products;
    }

    public function getProductsCookie ()
    {
        $products = [];
        $cookies = \Yii::$app->request->cookies;
        if (($cookie = $cookies->get('products')) !== null) {
            $products = json_decode($cookie->value, true);
        }
        return $products;
    }

    public function cleanCartCookie()
    {
        $cookies2 = \Yii::$app->response->cookies;
        $cookies2->remove('products');
        $cookies2->remove('quantity');
    }
}
