<?php
use yii\helpers\Html;
?>
<h1>Shop</h1>

<div>
	<div style="float: left; width: 300px;">
		<?php foreach ($categories as $category) :
			echo Html::a($category->title, ['id' => $category->id], ['class' => 'btn']).'<br>';
			  endforeach; ?>
	</div>
	<div>
		<?php foreach ($products as $product) : ?>
			<div style="float: left; width: 250px; height: 200px;">
				<?= Html::a($product->title, ['id' => $product->id], ['class' => 'btn']) ?><br>
				<?= Html::a('Купить', [''], 
									  ['class' => 'btn btn-primary', 
									   'onClick' => 'addCart('.$product->id.'); return false;']) ?>
			</div>
		<?php endforeach; ?>
	</div>
</div>

<script type="text/javascript">
	function addCart(id) {
		$.ajax({
			method: "POST",
			data: {id: id},
			url: "/web/index.php?r=shop/addcart",
			dataType: "json",
			success: function (response) {
				$("#countProduct").text('Cart ('+response.quantity+')');
			}
	    });
	}
</script>