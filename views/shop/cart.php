<?php
use yii\helpers\Html;
?>
<h1>Cart</h1>

<div>
	<table width='100%'>
		<tr>
			<th>Наименование</th>
			<th>Количество</th>
			<th>Цена</th>
			<th>Сумма</th>
		</tr>	
	<?php $all = 0;
		  foreach ($products as $product) : 
		  $all += $carts[$product->id]*$product->price; 
	?>
		<tr>
			<td><?=$product->title?></td>
			<td><?=$carts[$product->id]?></td>
			<td><?=$product->price?></td>
			<td><?=($carts[$product->id]*$product->price)?></td>
		</tr>
	<?php endforeach; ?>
	<tr><td></td><td></td><td><strong>Всего</strong></td><td><strong><?=$all?></strong></td></tr>
	</table><br>
	<?= Html::a('Оформить заявку', ['#'], ['class'=>'btn btn-primary',
										   'onClick'=>'alert("К сожалению, пока нет продолжения!"); return false;']) ?>
</div>

